#pragma once
#include "Wezel.h"
#include <iostream>
#include <string>

using namespace std;

template <class T>
class Kontener
{
private:
	Wezel<T> *glowa;
	int liczbaElementow; 
	Wezel<T>* getElementFromIndex(int index);
public:
	bool komunikaty = false;
	Kontener();
	~Kontener();
	void push(const T &obiekt);
	void pop();
	void insert(const T &obiekt, int index);
	void remove(int index);
	void swap(int i, int j);
	void save();
	void load();
	Kontener<T>& operator=(const Kontener<T>& kon);
	Kontener<T>& operator+=(const Kontener<T>& kon);
	bool contains(const T &obiekt);
	void moveToEnd(int index);
	T operator[](int);
	void clear();
	bool empty();
	int size() { return this->liczbaElementow; }
	void wyswietl();
};

template <class T>
void Kontener<T>::clear()
{
	while (this->glowa != NULL)
	{
		Wezel<T> *temp = this->glowa;
		this->glowa = this->glowa->nast;
		delete temp;
	}
	this->liczbaElementow = 0;
}

template <class T>
Kontener<T>::Kontener()
{
	this->glowa = NULL;
	this->liczbaElementow = 0;
}


template <class T>
Kontener<T>::~Kontener()
{
	this->clear();
}

template <class T>
void Kontener<T>::push(const T &obiekt)
{
	Wezel<T> *nowy = new Wezel<T>(obiekt);
	if (this->glowa == NULL)
		this->glowa = nowy;
	else
	{
		Wezel<T> *temp = this->glowa;
		while (temp->nast != NULL)
		{
			temp = temp->nast;
		}
		temp->nast = nowy;
		nowy->poprz = temp;
	}
	this->liczbaElementow++;
}

template <class T>
void Kontener<T>::pop()
{
	if (this->glowa == NULL)
	{
		string wyjatek = "Kontener jest pusty!!!\n";
		throw wyjatek;
	}
	Wezel<T> *last;
	last = this->getElementFromIndex(this->liczbaElementow - 1);
	if (last != this->glowa)
		last->poprz->nast = NULL;
	else
		this->glowa = NULL;
	delete last;
	this->liczbaElementow--;
	if(this->komunikaty)
		cout << "Zdjeto ostatni element z listy" << endl;
}

//funkcja zwracajaca obiekt o podanym indeksie
//od razu sprawdza czy kontener nie jest pusty, czy indeks nie jest niepoprawny i wyrzuca wyj�tek
template<class T>
Wezel<T>* Kontener<T>::getElementFromIndex(int index)
{
	string wyjatek = "";
	if (this->glowa == NULL)
	{
		wyjatek = "Kontener jest pusty!!!\n";
		throw wyjatek;
	}
	if (index < 0)
	{
		wyjatek = "Indeks nie moze byc ujemny!!!\n";
		throw wyjatek;
	}
	Wezel<T> *temp = this->glowa;
	int i = 0;
	while (i < index)
	{
		temp = temp->nast;
		i++;
		if (temp == NULL)
		{
			wyjatek = "Podany indeks wykracza poza liste!!!\n";
			throw wyjatek;
		}
	}
	return temp;
}

template<class T>
inline void Kontener<T>::insert(const T & obiekt, int index)
{
	Wezel<T> *w, *nowy;
	w = this->getElementFromIndex(index);
	nowy = new Wezel<T>(obiekt);
	if (w == this->glowa)
	{
		this->glowa->poprz = nowy;
		nowy->nast = this->glowa;
		this->glowa = nowy;
	}
	else
	{
		nowy->nast = w;
		nowy->poprz = w->poprz;
		w->poprz->nast = nowy;
		w->poprz = nowy;
	}
	this->liczbaElementow++;
}

template<class T>
void Kontener<T>::remove(int index)
{
	Wezel<T> *w;
	try
	{
		w = this->getElementFromIndex(index);
		if (w == this->glowa)
		{
			this->glowa = this->glowa->nast;
			this->glowa->poprz = NULL;
			delete w;
		}
		else if (w->nast == NULL)
		{
			w->poprz->nast = NULL;
			delete w;
		}
		else
		{
			delete w;
		}
		if (this->komunikaty)
			cout << "Usunieto element o indeksie " << index << endl;
		this->liczbaElementow--;
	}
	catch (string s)
	{
		cout << s;
	}
}

template<class T>
void Kontener<T>::swap(int i, int j)
{
	Wezel<T> *w1, *w2;
	try
	{
		w1 = getElementFromIndex(i);
		w2 = getElementFromIndex(j);

		/*Wezel<T> *w1p = w1->poprz, *w1n = w1->nast;


		if (abs(i - j) > 1)
		{
			if (w1->poprz != NULL)
				w1->poprz->nast = w2;
			if (w1->nast != NULL)
				w1->nast->poprz = w2;
			w1->nast = w2->nast;
			w1->poprz = w2->poprz;
			if (w2->poprz != NULL)
				w2->poprz->nast = w1;
			if (w2->nast != NULL)
				w2->nast->poprz = w1;
			w2->nast = w1n;
			w2->poprz = w1p;
		}
		else
		{
			if (w1->poprz != NULL)
				w1->poprz->nast = w2;
			w1->nast = w2->nast;
			w1->poprz = w2;
			if (w2->nast != NULL)
				w2->nast->poprz = w1;
			w2->poprz = w1p;
			w2->nast = w1;
		}*/
		T temp;
		temp = w1->obiekt;
		w1->obiekt = w2->obiekt;
		w2->obiekt = temp;

		if (this->komunikaty)
			cout << "Poprawnie zamieniono element " << i << " z elementem " << j << endl;
	}
	catch (string s)
	{
		cout << s;
	}
}

template<class T>
void Kontener<T>::save()
{
	string nazwa, wyjatek;

	if (this->glowa == NULL)
	{
		wyjatek = "Kontener jest pusty!!!\n";
		throw wyjatek;
	}

	ofstream plik;
	cout << "Podaj nazwe pliku: ";
	cin >> nazwa;
	plik.open(nazwa);
	if (plik.is_open())
	{
		Wezel<T> *temp = this->glowa;
		while (temp != NULL)
		{
			plik << temp->obiekt;
			if (temp->nast != NULL)
				plik << endl << endl;
			temp = temp->nast;
		}
		if (this->komunikaty)
			cout << "Poprawnie zapisano kontener do pliku" << endl;
	}
	else
	{
		wyjatek = "Blad tworzenia pliku do zapisu!\n";
		throw wyjatek;
	}
}

template<class T>
void Kontener<T>::load()
{
	string nazwa, wyjatek;
	ifstream plik;
	cout << "Podaj nazwe pliku: ";
	cin >> nazwa;
	plik.open(nazwa);
	if (plik.is_open())
	{
		while (!plik.eof())
		{
			T nowy;
			plik >> nowy;
			this->push(nowy);
		}
		if (this->komunikaty)
			cout << "Poprawnie zaladowano kontener z pliku" << endl;
	}
	else
	{
		wyjatek = "Brak takiego pliku!\n";
		throw wyjatek;
	}
}

template<class T>
Kontener<T>& Kontener<T>::operator=(const Kontener<T>& kon)
{
	this->clear();
	this->liczbaElementow = kon.liczbaElementow;
	Wezel<T> *temp = kon.glowa;
	while (temp != NULL)
	{
		this->push(temp->obiekt);
		temp = temp->nast;
	}
	return *this;
}

template<class T>
Kontener<T>& Kontener<T>::operator+=(const Kontener<T>& kon)
{
	Wezel<T> *temp = kon.glowa;
	for (int i = 0; i < kon.liczbaElementow; i++)
	{
		this->push(temp->obiekt);
		temp = temp->nast;
	}
	return *this;
}

template<class T>
bool Kontener<T>::contains(const T & obiekt)
{
	Wezel<T> *temp = this->glowa;
	while (temp != NULL && !(temp->obiekt == obiekt))
		temp = temp->nast;
	if (temp == NULL)
		return false;
	else
		return true;
}

template<class T>
void Kontener<T>::moveToEnd(int index)
{
	Wezel<T> *w, *last;
	try 
	{
		w = this->getElementFromIndex(index);
		last = this->getElementFromIndex(this->liczbaElementow - 1);
		if (w == this->glowa)
		{
			last->nast = w;
			glowa = glowa->nast;
			glowa->poprz = NULL;
			w->nast = NULL;
			w->poprz = last;
		}
		else if (w == last);
		else
		{
			last->nast = w;
			w->poprz->nast = w->nast;
			w->nast->poprz = w->poprz;
			w->nast = NULL;
			w->poprz = last;
		}
		if (this->komunikaty)
			cout << "Poprawnie przeniesiono element " << index << " na koniec kontenera" << endl;
	}
	catch (string s)
	{
		cout << s;
	}
}

template<class T>
T Kontener<T>::operator[](int index)
{
	Wezel<T> *w;
	try
	{
		w = this->getElementFromIndex(index);
	}
	catch (string s)
	{
		cout << s;
	}
	return w->obiekt;
}

template<class T>
bool Kontener<T>::empty()
{
	if (this->liczbaElementow > 0)
		return false;
	else
		return true;
}

template <class T>
void Kontener<T>::wyswietl()
{
	if (this->glowa == NULL)
	{
		string wyjatek = "Kontener jest pusty!!!\n";
		throw wyjatek;
	}
	Wezel<T> *temp = this->glowa;
	while (temp != NULL)
	{
		cout << temp->obiekt;
		cout << endl;
		temp = temp->nast;
	}
}

