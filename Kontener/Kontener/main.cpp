#include "Kontener.h"
#include "Cukiernia.h"
#include "Produkt.h"

using namespace std;

Produkt* zrobListe(Kontener<string> *nazwy_prod, Kontener<float> *cena_prod)
{
	Produkt *lista = new Produkt[nazwy_prod->size()];
	for (int i = 0; i < nazwy_prod->size(); i++)
	{
		lista[i].setCena((*cena_prod)[i]);
		lista[i].setNazwa((*nazwy_prod)[i]);
	}

	return lista;
}

int main()
{
	Kontener<Cukiernia> *aktywna;
	Kontener<Cukiernia> cukiernie1;
	Kontener<Cukiernia> cukiernie2;
	cukiernie1.komunikaty = true;
	cukiernie2.komunikaty = true;

	aktywna = &cukiernie1;

	int opcja = 0;
	cin.exceptions(istream::failbit | istream::badbit);

	while (opcja != 13)
	{
		cout << "AKTYWNY KONTENER: ";
		if (aktywna == &cukiernie1)
			cout << "Cukiernie1" << endl;
		else
			cout << "Cukiernie2" << endl;
		cout << "MENU" << endl;
		cout << "1. Dodaj obiekt do kontenera (metody push i insert)" << endl;
		cout << "2. Zapisz kontener do pliku" << endl;
		cout << "3. Wczytaj kontener z pliku" << endl;
		cout << "4. Wyswietl kontener" << endl;
		cout << "5. Zdejmij ostatni element (metoda pop)" << endl;
		cout << "6. Usun element (metoda remove)" << endl;
		cout << "7. Zamien elementy (metoda swap)" << endl;
		cout << "8. Przenies element na koniec (metoda moveToEnd)" << endl;
		cout << "9. Zmien kontener" << endl;
		cout << "10. Dodaj kontener do konetenera (operator +=)" << endl;
		cout << "11. Przypisz kontener do kontenera (operator =)" << endl;
		cout << "12. Sprawdz czy kontener zawiera obiekt (metoda contains)" << endl;
		cout << "13. Zakoncz program" << endl << endl;
		cout << "Wybrana opcja: ";
		try
		{
			cin >> opcja;
		}
		catch (exception e)
		{
			opcja = 0;
			//te 2 linijki s� po to �eby cin nie zap�tla� si�
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
		switch (opcja)
		{
			case 1:
			{
				if (aktywna->size() == 0)
					opcja = 1;
				else
				{
					cout << endl << "Ktora metode chcesz wywolac?: " << endl;
					cout << "1. Metoda push" << endl;
					cout << "2. Metoda insert" << endl;
					cout << "Twoj wybor: ";
					try
					{
						cin >> opcja;
					}
					catch (exception e)
					{
						opcja = 0;
						//te 2 linijki s� po to �eby cin nie zap�tla� si�
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
					}
				}
				string nazwa = "";
				float cena;
				int lprac, index;
				string nazwaCukiernii;
				Cukiernia *temp;
				Kontener<string> *nazwy_prod = new Kontener<string>();
				Kontener<float> *ceny_prod = new Kontener<float>();

				switch (opcja)
				{
				case 1:
					cout << "Podaj nazwe cukierni: ";
					cin >> nazwaCukiernii;

					cout << "Podawaj nazwy produkt�w wraz cenami (wcisniecie 0 konczy wprowadzanie): " << endl;
					while (nazwa != "0")
					{
						cin >> nazwa;
						if (nazwa != "0")
						{
							nazwy_prod->push(nazwa);
							cin >> cena;
							ceny_prod->push(cena);
						}
					}
					if (!nazwy_prod->empty())
					{
						cout << "Podaj liczbe pracownikow: ";
						cin >> lprac;
						temp = new Cukiernia(zrobListe(nazwy_prod, ceny_prod), nazwy_prod->size(), lprac);
						temp->setNazwa(nazwaCukiernii);
						aktywna->push(*temp);
					}
					else
					{
						cout << "Nie mozna utworzyc obiektu bez podania produktow!!!" << endl;
						system("pause");
					}
					break;
				case 2:
					cout << "Podaj nazwe cukierni: ";
					cin >> nazwaCukiernii;

					cout << "Podawaj nazwy produkt�w wraz cenami (wcisniecie 0 konczy wprowadzanie): " << endl;
					while (nazwa != "0")
					{
						cin >> nazwa;
						if (nazwa != "0")
						{
							nazwy_prod->push(nazwa);
							cin >> cena;
							ceny_prod->push(cena);
						}
					}
					if (!nazwy_prod->empty())
					{
						cout << "Podaj liczbe pracownikow: ";
						cin >> lprac;
						temp = new Cukiernia(zrobListe(nazwy_prod, ceny_prod), nazwy_prod->size(), lprac);
						temp->setNazwa(nazwaCukiernii);
						cout << "Podaj indeks, na ktory chcesz wstawic obiekt: ";
						cin >> index;
						try
						{
							aktywna->insert(*temp, index);
						}
						catch (string s)
						{
							delete temp;
							cout << s;
							system("pause");
						}
					}
					else
					{
						cout << "Nie mozna utworzyc obiektu bez podania produktow!!!" << endl;
						system("pause");
					}
					break;
				default:
					cout << "Bledna wartosc! Wpisz liczbe z zakresu 1-2" << endl;
					system("pause");
					break;
				}
			}
		break;

			case 2:
				try
				{
					aktywna->save();
				}
				catch (string s)
				{
					cout << s;
				}
				system("pause");
				break;

			case 3:
				try
				{
					aktywna->clear();
					aktywna->load();
				}
				catch (string s)
				{
					cout << s;
				}
				system("pause");
				break;

			case 4:
				try
				{
					aktywna->wyswietl();
				}
				catch (string s)
				{
					cout << s;
				}
				system("pause");
				break;

			case 5:
				try
				{
					aktywna->pop();
				}
				catch (string s)
				{
					cout << s;
				}
				system("pause");
				break;

			case 6:
				try
				{
					int index;
					cout << "Podaj indeks elementu do usuniecia: ";
					cin >> index;
					aktywna->remove(index);
				}
				catch (string s)
				{
					cout << s;
				}
				catch (exception e)
				{
					cout << "Podaj wartosc liczbowa!!!" << endl;
					//te 2 linijki s� po to �eby cin nie zap�tla� si�
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				system("pause");
				break;

			case 7:
				try
				{
					int i, j;
					cout << "Podaj indeksy elementow, ktore chcesz zamienic: ";
					cin >> i;
					cin >> j;
					aktywna->swap(i, j);
				}
				catch (string s)
				{
					cout << s;
				}
				catch (exception e)
				{
					cout << "Podaj wartosci liczbowe!!!" << endl;
					//te 2 linijki s� po to �eby cin nie zap�tla� si�
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				system("pause");
				break;

			case 8:
				try
				{
					int index;
					cout << "Podaj indeks elementu, ktory chcesz przeniesc na koniec: ";
					cin >> index;
					aktywna->moveToEnd(index);
				}
				catch (string s)
				{
					cout << s;
				}
				catch (exception e)
				{
					cout << "Podaj wartosc liczbowa!!!" << endl;
					//te 2 linijki s� po to �eby cin nie zap�tla� si�
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				system("pause");
				break;

			case 9:
				if (aktywna == &cukiernie1)
					aktywna = &cukiernie2;
				else
					aktywna = &cukiernie1;
				break;

			case 10:
				cout << endl << "Ktora operacje chcesz wykonac: " << endl;
				cout << "1. cukiernie1 += cukiernie2" << endl;
				cout << "2. cukiernie2 += cukiernie1" << endl;
				cout << "Twoj wybor: ";
				try
				{
					cin >> opcja;
				}
				catch (exception e)
				{
					opcja = 0;
					//te 2 linijki s� po to �eby cin nie zap�tla� si�
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				switch (opcja)
				{
					case 1:
						cukiernie1 += cukiernie2;
						break;
					case 2:
						cukiernie2 += cukiernie1;
						break;
					default:
						cout << "Bledna wartosc! Wpisz liczbe z zakresu 1-2" << endl;
						system("pause");
						break;
				}
				break;

			case 11:
				cout << endl << "Ktora operacje chcesz wykonac: " << endl;
				cout << "1. cukiernie1 = cukiernie2" << endl;
				cout << "2. cukiernie2 = cukiernie1" << endl;
				cout << "Twoj wybor: ";
				try
				{
					cin >> opcja;
				}
				catch (exception e)
				{
					opcja = 0;
					//te 2 linijki s� po to �eby cin nie zap�tla� si�
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				switch (opcja)
				{
				case 1:
					cukiernie1 = cukiernie2;
					break;
				case 2:
					cukiernie2 = cukiernie1;
					break;
				default:
					cout << "Bledna wartosc! Wpisz liczbe z zakresu 1-2" << endl;
					system("pause");
					break;
				}
				break;

			case 12:
				{
					string nazwa;
					cout << "Podaj nazwe szukanego obiektu: ";
					cin >> nazwa;
					Cukiernia temp;
					temp.setNazwa(nazwa);
					if (aktywna->contains(temp))
						cout << "Kontener zawiera podany obiekt" << endl;
					else
						cout << "Kontener nie zawiera podanego obiektu" << endl;
					system("pause");
				}
				break;

			case 13:
				break;

			default:
				cout << "Bledna wartosc! Wpisz liczbe z zakresu 1-13" << endl;
				system("pause");
				break;
		}
		system("cls");
	}
	
	cout << "KONIEC PRACY PROGRAMU" << endl;

	system("pause");
	return 0;
}