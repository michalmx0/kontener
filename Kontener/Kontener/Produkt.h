#pragma once
#include <iostream>

using namespace std;

class Produkt
{
private:
	string nazwa;
	float cena;
public:
	Produkt();
	~Produkt();
	string getNazwa();
	float getCena();
	void setNazwa(string);
	void setCena(float);
};

