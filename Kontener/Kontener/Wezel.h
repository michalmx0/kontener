#pragma once
#include <iostream>

using namespace std;

template <class T>
class Wezel
{
	template <class T> friend class Kontener;
protected:
	T obiekt;
	Wezel *nast;
	Wezel *poprz;
public:
	Wezel(const T &obiekt);
	~Wezel();
};


template<class T>
Wezel<T>::Wezel(const T &obiekt)
{
	this->obiekt = obiekt;
	this->nast = NULL;
	this->poprz = NULL;
}


template <typename T>
Wezel<T>::~Wezel()
{
	if (this->poprz != NULL && this->nast != NULL)
	{
		this->poprz->nast = this->nast; //naprawianie po��czenia po usuni�ciu w�z�a
		this->nast->poprz = this->poprz; //
	}
}
