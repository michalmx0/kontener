#include "Cukiernia.h"

int Cukiernia::licznik= 0;

Cukiernia::Cukiernia()
{
#ifdef _DEBUG
	//cout << "Konstruktor domyslny" << endl;
#endif // _DEBUG
	licznik++;
	this->listaProduktow = NULL;
	this->liczbaPracownikow = 0;
	this->liczbaProduktow = 0;
}

Cukiernia::Cukiernia(Cukiernia &c)
{
#ifdef _DEBUG
	//cout << "Konstruktor kopiujacy" << endl;
#endif // _DEBUG
	this->nazwa = c.nazwa;
	this->liczbaPracownikow = c.liczbaPracownikow;
	this->liczbaProduktow = c.liczbaProduktow;
	this->listaProduktow = new Produkt[this->liczbaProduktow];
	for (int i = 0; i < this->liczbaProduktow; i++)
	{
		this->listaProduktow[i] = c.listaProduktow[i];
	}
	licznik++;
}

Cukiernia::Cukiernia(Produkt *lista, int liczbaProd, int liczbaPrac)
{
#ifdef _DEBUG
	//cout << "Konstruktor parametryczny" << endl;
#endif // _DEBUG
	this->nazwa = nazwa;
	this->liczbaPracownikow = liczbaPrac;
	this->liczbaProduktow = liczbaProd;
	this->listaProduktow = new Produkt[this->liczbaProduktow];
	for (int i = 0; i < this->liczbaProduktow; i++)
	{
		this->listaProduktow[i] = lista[i];
	}
	licznik++;
}

Cukiernia::~Cukiernia()
{
#ifdef _DEBUG
	//cout << "Desktuktor" << endl;
#endif // _DEBUG
	delete[] this->listaProduktow;
	licznik--;
}

int Cukiernia::getLicznik()
{
	return licznik;
}

int Cukiernia::getLiczbaProduktow()
{
	return this->liczbaProduktow;
}

Cukiernia & Cukiernia::operator=(const Cukiernia &c)
{
	this->nazwa = c.nazwa;
	this->liczbaPracownikow = c.liczbaPracownikow;
	this->liczbaProduktow = c.liczbaProduktow;
	delete[] this->listaProduktow;
	this->listaProduktow = new Produkt[this->liczbaProduktow];
	for (int i = 0; i < this->liczbaProduktow; i++)
		this->listaProduktow[i] = c.listaProduktow[i];
	return *this;
}

Produkt Cukiernia::operator[](int i)
{
	return this->listaProduktow[i];
}

Cukiernia::operator int()
{
	return this->liczbaProduktow;
}

ostream & operator<<(ostream &out, Cukiernia &c)
{
	out << "Nazwa Cukiernii: " << c.nazwa << endl;
	out << "Lista Produktow: " << endl;
	if (c.liczbaProduktow == 0)
	{
		out << "Brak produktow!" << endl;
	}
	else
	{
		for (int i = 0; i < c.liczbaProduktow; i++)
		{
			out << i + 1 << ". ";
			out << c[i].getNazwa().c_str() << "   " << c[i].getCena() << " zl" << endl;
		}
	}
	out << "Liczba prawcownikow cukierni: " << c.liczbaPracownikow << endl << endl;
	return out;
}

bool operator<(const Cukiernia & l, const Cukiernia & r)
{
	if (l.liczbaPracownikow > r.liczbaPracownikow)
		return true;
	else
		return false;
}

bool operator==(const Cukiernia & l, const Cukiernia & r)
{
	if (l.nazwa.compare(r.nazwa) == 0)
		return true;
	else
		return false;
}

string zastapZnaki(string text, char a, char b) {
	for (string::iterator it = text.begin(); it != text.end(); ++it) {
		if (*it == a) {
			*it = b;
		}
	}
	return text;
}

ofstream& operator<<(ofstream &out, Cukiernia &c)
{
	out << "nazwa " << c.nazwa << endl;
	out << "liczbaProduktow " << c.liczbaProduktow << endl;
	out << "liczbaPracownikow " << c.liczbaPracownikow << endl;
	out << "listaProduktow ";
	for (int i = 0; i < c.liczbaProduktow; i++)
	{
		if (c[i].getNazwa().find(" ") != string::npos)
			out << endl << zastapZnaki(c[i].getNazwa(), ' ', '_') << " " << c[i].getCena();
		else
			out << endl << c[i].getNazwa() << " " << c[i].getCena();
	}
	return out;
}

ifstream& operator>>(ifstream &in, Cukiernia &c)
{
	string nazwa;
	int liczba;
	in >> nazwa;
	in >> nazwa;
	c.nazwa = nazwa;
	in >> nazwa;
	in >> liczba;
	c.liczbaProduktow = liczba;
	in >> nazwa;
	in >> liczba;
	c.liczbaPracownikow = liczba;
	in >> nazwa;
	c.listaProduktow = new Produkt[c.liczbaProduktow];
	for (int i = 0; i < c.liczbaProduktow; i++)
	{

		Produkt nowy;
		float cena;
		in >> nazwa;
		nazwa = zastapZnaki(nazwa, '_', ' ');
		in >> cena;
		nowy.setCena(cena);
		nowy.setNazwa(nazwa);
		c.listaProduktow[i] = nowy;
	}
	return in;
}

Cukiernia& Cukiernia::operator+=(const Cukiernia& p)
{
	int staryRozmiar = this->liczbaProduktow;
	this->liczbaProduktow += p.liczbaProduktow;
	Produkt *nowa = new Produkt[this->liczbaProduktow];
	for (int i = 0; i < staryRozmiar; i++)
		nowa[i] = this->listaProduktow[i];
	for (int i = staryRozmiar; i < this->liczbaProduktow; i++)
		nowa[i] = p.listaProduktow[i-staryRozmiar];
	delete[] this->listaProduktow;
	this->listaProduktow = nowa;
	return *this;
}
