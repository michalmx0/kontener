#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "Produkt.h"

using namespace std;

class Cukiernia
{
private:
	string nazwa;
	Produkt *listaProduktow;
	int liczbaProduktow;
	int liczbaPracownikow;
	static int licznik;
public:
	Cukiernia(); //konstruktor domy�lny
	Cukiernia(Cukiernia&); //konstruktor kopiuj�cy
	Cukiernia(Produkt*, int, int); //konstruktor parametryczny
	~Cukiernia(); //destruktor
	static int getLicznik();
	int getLiczbaProduktow();
	Cukiernia& operator=(const Cukiernia&); //operator przypisania
	Produkt operator[](int); //operator indeksowy
	operator int(); //operator konwersji
	friend ostream& operator<<(ostream&, Cukiernia&); //operator wpisania na strumie�
	Cukiernia& operator+=(const Cukiernia& p); //opertor jednoargumentowy
	friend bool operator<(const Cukiernia& l, const Cukiernia& r); //operator dwuargumentowy
	friend bool operator==(const Cukiernia& l, const Cukiernia& r);
	friend ofstream& operator<<(ofstream&, Cukiernia&);
	friend ifstream& operator>>(ifstream&, Cukiernia&);
	void setNazwa(string nazwa) { this->nazwa = nazwa; }
};

